Rails.application.routes.draw do
  devise_for :users, :controllers => {registrations: 'registrations'}

  root to: "welcome_index#index", as: "/home"

  get "password/reset", to: "password_reset#new"
  post "password/reset", to: "password_reset#create"
  get "password/reset/edit", to: "password_reset#edit"
  patch "password/reset/edit", to: "password_reset#update"

  get "/profile", to: "user_profile#user_profile"
  get "/profile/edit", to: "user_profile#edit"

end
