class PasswordResetController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(email: params[:email])

    if @user.present?
      # send email
      PasswordMailer.with(user: @user).reset.deliver_now
    else
      puts "SOMETHING WRONG WAS HEPPEND | WE CANT SEND YOU A TOKEN"
    end

    redirect_to home_path, notice: "If an account with that email was found, we have sent a link to reset your password."
  end

  def edit
    @user = User.find_signed!(params[:token], purpose: "password_reset")
  rescue ActiveSupport::MessageVerifier::InvalidSignature
    redirect_to sign_in_path, notice: "Your token has expired. Please try again."
  end

  def update
    @user = User.find_signed(params[:token], purpose: "password_reset")
    if @user.update(password_params)
      redirect_to new_user_session_path, notice: "Your password was reset successfully."
    else
      render :edit, notice: "Something went wrong with your password. Remember, passwords should be longer than 8 characters and should match."
    end
  end

  private
  def password_params
    params.require(:user).permit(:password, :password_confirmation)
  end

end
