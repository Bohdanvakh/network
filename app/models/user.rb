class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validate :validate_username

  validates :email,
        uniqueness: { message: " is already taken." },
        on: :some_action

  validates :password,
        presence: true,
        on: :some_action

  validates :password_confirmation,
        presence: { message: " must be provided" }


  def validate_username
    if username.length < 6
      errors.add(:username, "must be provided and have at least 6 characters")
    end
  end
end
